$(document).ready(function() {
	// **********Code for .addClass ******************
	function add_class () {
		$( "p" ).addClass( "bg-chnge" );
		$( "p:last" ).addClass( "selected highlight" );
		$( "div" ).addClass(function( index, currentClass ) {
		  var addedClass;

		  if ( currentClass === "main" ) {
		    addedClass = "green";
		    $( "p:last" ).text( "There is one green div" );
		  }
		  return addedClass;
		});

	}
	add_class ();

	// **********Code for .removeClass ******************
	function remove_class () {
		$( "p:even" ).removeClass( "under" );
		$( "p:odd" ).removeClass( "blue" );
	}
	remove_class ();

// **********Code for .css ******************
	$( "p" ).on( "mouseover", function() {
  $( this ).css( "color", "#0f0" );
	});

	$( "p" ).on( "mouseout", function() {
  $( this ).css( "color", "" );
	});

// **********Code for .hasClass ******************
	var str1 = $("p:first").hasClass("bule");
	var str2 = $("p:last").hasClass("under");
	// alert(str1);
	// alert(str2);

// **********Code for .height() ******************
	var hgt1 = $("header .wrapper").height();
	var hgt2 = $(".main .wrapper").height();
	// alert(hgt1+"px");
	// alert(hgt2+"px");

// **********Code for .width() ******************
	var wdth1 = $(document).width();
	var wdth2 = $(window).width();
	// alert(wdth1+"px");
	// alert(wdth2+"px");

// **********Code for .offset() ******************
	var p = $( "p:last" );
	var offset = p.offset();
	p.html( "left: " + offset.left + ", top: " + offset.top );

// **********Code for .position() ******************
	var p = $( "p:last" );
	var position = p.position();
	p.html( "left: " + position.left + ", top: " + position.top );

// **********Code for .scrollTop() ******************
	var p = $( "p:first" );
	$( "p:last" ).text( "scrollTop:" + p.scrollTop() );

// **********Code for .scrollLeft() ******************
	var p = $( "p:first" );
	$( "p:last" ).text( "scrollLeft:" + p.scrollLeft() );

// **********Code for .toggleClass() ******************
	$( "p:last" ).click(function() {
	  $( this ).toggleClass( "highlight" );
	});

});

