$(document).ready(function() {

// **********Code for .toggleClass() ******************
	$( "p:last" ).click(function() {
	  $( this ).toggleClass( "highlight" );
	});

// **********Code for .resize() ******************
	$( window ).resize(function() {
	  $( "p:last" ).append( "<span>Handler for .resize() called.</span>" );
	});

// **********Code for .scroll() ******************
	$( window ).scroll(function() {
	  $( "span" ).css( "display", "inline" ).fadeOut( "slow" );
	});

// **********Code for .load() ******************
	$( window ).load(function() {
	  if ( $( "img" ).height() > 100) {
	    $( "img" ).addClass( "bigImg" );
	  }
	});

// **********Code for .unload() ******************
	$( window ).unload(function() {
	  return "Bye now!";
	});

// **********Code for .bind() ******************
	$( "p" ).bind( "click", function( event ) {
	  var str = "( " + event.pageX + ", " + event.pageY + " )";
	  $( ".bindspan" ).text( "Click happened! " + str );
	});
	$( "p" ).bind( "dblclick", function() {
	  $( ".bindspan" ).text( "Double-click happened in " + this.nodeName );
	});
	$( "p" ).bind( "mouseenter mouseleave", function( event ) {
	  $( this ).toggleClass( "over" );
	});

// **********Code for .delegate() ******************
	$( "body" ).delegate( "p", "click", function() {
	  $( this ).after( "<p>Another paragraph!</p>" );
	});

// **********Code for .on() and .off() ******************

	function flash() {
	  $( "div" ).show().fadeOut( "slow" );
	}
	$( "#bind" ).click(function() {
	  $( "body" )
	    .on( "click", "#theone", flash )
	    .find( "#theone" )
	    .text( "Can Click!" );
	});
	$( "#unbind" ).click(function() {
	  $( "body" )
	    .off( "click", "#theone", flash )
	    .find( "#theone" )
	    .text( "Does nothing..." );
	});

// **********Code for .trigger() and .triggerHandler() ******************
	$( "#old" ).click(function() {
	  $( "input" ).trigger( "focus" );
	});
	$( "#new" ).click(function() {
	  $( "input" ).triggerHandler( "focus" );
	});
	$( "input" ).focus(function() {
	  $( "<span>Focused!</span>" ).appendTo( "body" ).fadeOut( 1000 );
	});

});

